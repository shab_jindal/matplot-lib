#!/usr/bin/env python
# coding: utf-8

# In[75]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[76]:


ins_data = pd.read_csv(r"C:\Users\ritish\Downloads\insurance.csv")


# In[77]:


ins_data.head()


# In[78]:


ins_data.head(20)


# In[79]:


ins_data.tail()


# In[80]:


ins_data.count()


# In[81]:


ins_data['sex'].value_counts()


# In[82]:


ins_data['smoker'].value_counts()


# In[83]:


#Plots in matplotlib reside within a figure object, use plt.figure to create new figure
fig=plt.figure()


# In[85]:


ax.hist(ins_data['age'],bins = 5)


# In[129]:


fig, ax = plt.subplots()

a_heights, a_bins = np.histogram(ins_data['charges'])
b_heights, b_bins = np.histogram(ins_data['age'], bins=a_bins)

width = (a_bins[1] - a_bins[0])/3

ax.bar(a_bins[:-1], a_heights, width=width, facecolor='cornflowerblue')
ax.bar(b_bins[:-1]+width, b_heights, width=width, facecolor='seagreen')
#seaborn.despine(ax=ax, offset=10)


# In[87]:


ins_data['age'].hist()


# In[88]:


ins_data['smoker'].hist()


# In[101]:


ins_data[["age", "sex", "charges", "smoker"]].hist()

plt.show()


# In[ ]:


data = ins_data


# In[92]:


plt.plot(ins_data['age'])


# In[91]:


plt.plot(ins_data['charges'])


# In[126]:


#Grouping data by age:
grouped = ins_data.groupby(['sex','smoker'])

A0 = grouped.size()

A0

#and then plot it using:

A0.plot(kind='bar',facecolor = 'yellow')


# In[134]:


import matplotlib.pyplot as plt
import pandas as pd

# a scatter plot comparing children and smoker
ins_data.plot(kind='scatter',x='age',y='charges',color='red')
plt.show()


# In[140]:


import matplotlib.pyplot as plt
import pandas as pd

# a scatter plot comparing children and smoker
ins_data.plot(kind='scatter',x='age',y='bmi',color='green')
plt.show()


# In[148]:


x = 'age'
ys = ['bmi','charges','region','smoker']
plt.figure()
for charges in ys:
    plt.plot(x, charges, 'o')
plt.show()


# In[ ]:




